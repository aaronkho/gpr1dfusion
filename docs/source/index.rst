.. GRP1D documentation master file, created by
   sphinx-quickstart on Fri Jun 15 09:04:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

Contents:
---------
.. toctree::
   :maxdepth: 2
   :glob:

   *


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
