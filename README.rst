gpr1dfusion
===========

Installing the gpr1dfusion wrapper
----------------------------------

*Author: Aaron Ho (11/04/2019)*

Installation is **mandatory** for this package!

For first time users, it is strongly recommended to use the GUI
developed for this Python package. To obtain the Python package
dependencies needed to use this capability, install this package
by using the following on the command line::

    pip install [--user] gpr1dfusion

Use the :code:`--user` flag if you do not have root access on the system
that you are working on. If you have already cloned the
repository, enter the top level of the repository directory and
use the following instead::

    pip install [--user] -e .

Documentation
=============

Documentation of the equations used in the algorithm, along with
the available kernels and optimizers, can be found in docs/.
Documentation of the GPR1D module can be found on
`GitLab pages <https://aaronkho.gitlab.io/GPR1D>`_
